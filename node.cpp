#include <list>
#include <algorithm>
#include <random>
#include <cmath>

#include <iostream>
#include "node.h"
using std::list;
using std::uniform_real_distribution;
using std::mt19937;
using std::iterator;
using std::uniform_int_distribution; 
using std::advance;
using std::vector;
using std::cout;
using std::endl;
using std::default_random_engine;

int node::populatePackets(double simTime, int arrivalRate, default_random_engine& generator, uniform_real_distribution<double>& dis)
{
	double currentTime = 0.0;
	float uniformRandomVariable = 0;
	double interval = 0.0;
	int numPackets = 0;

	//uniform_real_distribution<double>dist(0,1);
	//default_random_engine gen;

	while (currentTime < simTime)
	{	
		uniformRandomVariable = log(1-dis(generator));
		interval = -1 * (uniformRandomVariable / arrivalRate);
		currentTime += interval;
		if (currentTime < simTime)
		{
			testPackets.push_back(currentTime);
			numPackets++;
		}
	}
	return numPackets;
}

void node::addPacket(double time)
{
	testPackets.push_back(time); 
}

double node::getFirstPacket(){
	return testPackets.front();
}

void node::removeFirstPacket(bool dropped){
	it = testPackets.begin();
       	double transDelay = 0.015, currentPacketTime = 0.0; 	
	if(!dropped)
		currentPacketTime = *it; // transDelay;
//		currentPacketTime = *it; 
        else
		currentPacketTime = *it;	
	advance(it,1);
	if (*it <= currentPacketTime)
	       *it = currentPacketTime;	
	//testPackets.erase(testPackets.begin());
	testPackets.pop_front();
	collisionCounter = 0;
        if (!persistent)
		busBusyCounter = 0;
	if (!dropped)
		temptCounter++; 	
}

uint node::getNumberOfPackets(){
	return (uint)testPackets.size();
}

bool node::checkBusy(double currentPacketSenseTime, double transDelay, default_random_engine& generator){
	bool isBusy = false;
	double bitTime = 0.000001, backoffTime = 0.0, lastTimeStamp = 0.0;
	it = testPackets.begin();

	if (getFirstPacket() >= currentPacketSenseTime && getFirstPacket() < (currentPacketSenseTime + transDelay))
	{
		isBusy = true;
		busBusyCounter ++;

//		cout<<"Busy"<<endl;

		if (busBusyCounter <= 10 || persistent)
		{
			//busy
			//update first packet in node's queue

			if (!persistent)
			{
				uniform_int_distribution<int> dis(0,(pow(2,collisionCounter)-1));
				backoffTime = dis(generator) * bitTime * 512;
			//	cout<<"backOffTime"<<backoffTime<<endl;
				*it = currentPacketSenseTime + backoffTime;
			//	*it += backoffTime;
			}
			else
			{	
				*it = currentPacketSenseTime + transDelay;
			}

			//lastTimeStamp = *it;
			//advance(it,1);
			/*
			while (it != testPackets.end()&& (*it < lastTimeStamp + transDelay))
			{
				*it =  lastTimeStamp + transDelay;
				lastTimeStamp = *it;
				advance(it,1);
			}*/
		}
		else
		{   
			removeFirstPacket(true);
			busBusyCounter = 0;
			isBusy = false;
		}
	}
	return isBusy;
}

bool node::checkCollision (double currentPacketSenseTime, double transDelay, default_random_engine& generator)
{
	bool collided = false;

	if (getFirstPacket() < currentPacketSenseTime)
	{
		collided = true;

//		cout<<"collision"<<endl;

		handleCollision(false, currentPacketSenseTime, generator, transDelay);
	}
	return collided;
}

void node::handleCollision (bool sender, double currentPacketSenseTime, default_random_engine& generator, double transDelay)
{
	double backoffTime = 0.0, firstPacketTransTime = 0.0, bitTime = 0.000001, lastTimeStamp = 0.0;
	it = testPackets.begin();

	collisionCounter++;
	temptCounter++; 

	//May be slow and need optimization
	uniform_int_distribution<int> dis(0,(pow(2,collisionCounter)-1));
	//

	if (collisionCounter <= 10)
	{
		backoffTime = dis(generator) * bitTime * 512;
		//firstPacketTransTime = currentPacketSenseTime + backoffTime;
		firstPacketTransTime = *it + backoffTime;
		//cout.precision(7);
		//cout<<std::fixed<<"backoffTime: "<<backoffTime<<endl;
		*it = firstPacketTransTime;
		//lastTimeStamp = firstPacketTransTime;
		//advance(it, 1);
		/*while (it != testPackets.end() && (*it < lastTimeStamp + transDelay))
		{
			*it = lastTimeStamp + transDelay;
			lastTimeStamp = *it;
			advance(it,1);
		}*/

	}
	else
	{
		collisionCounter = 0;
		removeFirstPacket(true);
	}


}

unsigned int node::nodeSize(){
	//bool same = distance(testPackets.begin(), testPackets.end()) == testPackets.size();
	//cout<<"distance() = testPackets.size() "<< same<<endl;
	return distance(testPackets.begin(), testPackets.end());
//	return testPackets.size();
}
