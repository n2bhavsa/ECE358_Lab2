CXX=g++
CXXFLAGS= -std=gnu++11 -o0 -I.
DEPS = node.h 
OBJ = main.o node.o
CSV = 

%.o: %.c $(DEPS)
		$(CXX) -c -o $@ $< $(CFLAGS)

default: main.o node.o 
		$(CXX) -o lab2Output main.o node.o 
clean: 
	rm -f $(OBJ) lab2Output nonpersistent_results.csv persistent_results.csv
cleanall:
	rm -f $(OBJ) $(CSV) lab2Output

