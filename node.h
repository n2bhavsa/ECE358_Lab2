#ifndef node_h
#define node_h
#include <list>
#include <algorithm>
#include <random>
#include <cmath>

#include <iostream>
using std::list;
using std::uniform_real_distribution;
using std::mt19937;
using std::iterator;
using std::uniform_int_distribution;
using std::advance;
using std::vector;
using std::default_random_engine;
//#include <stdio.h>

class node {
	private:
		bool persistent;
		list<double>::iterator it;
	public:
		list<double> testPackets;
		int collisionCounter;
		int busBusyCounter;
	        int temptCounter; 	
		node(double simTime, int arrivalRate, default_random_engine& generator, uniform_real_distribution<double>& dis, bool inputPersistent){
			collisionCounter = busBusyCounter = temptCounter = 0;
			persistent = inputPersistent;
			populatePackets(simTime, arrivalRate, generator, dis);
		}


		void addPacket(double time);
		int populatePackets(double simTime, int arrivalRate, default_random_engine& generator, uniform_real_distribution<double>& dis);
		double getFirstPacket();
		void removeFirstPacket(bool dropped);
		uint getNumberOfPackets();
		bool checkBusy(double currentPacketSenseTime, double transDelay, default_random_engine& generator);
		bool checkCollision (double currentPacketSenseTime, double transDelay, default_random_engine& generator);
		void handleCollision (bool sender, double currentPacketSenseTime, default_random_engine& generator, double transDelay);
		unsigned int nodeSize();
};
#endif /* node_h */
