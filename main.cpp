#include "node.h"

#include <list>
#include <algorithm>
#include <random>
#include <cmath>
#include <iostream>
#include <fstream>
#include <iterator>

using std::distance;
using std::uniform_real_distribution;
using std::mt19937;
using std::default_random_engine;
using std::list;
using std::cout;
using std::endl;
using std::advance;
using std::ofstream;
/*
 * Debug method to print packets in one node
 * a: List of nodes transmitting in the network
 * nodeIndex: Index of node to be printed
 */
void printNode (list<node> &a, int nodeIndex){
	//cout<<endl;
	list<node>::iterator currentNode = a.begin();
	advance(currentNode, nodeIndex);
	list<double>::iterator packetIterator = currentNode->testPackets.begin();

	int index = 0;

	while (packetIterator != currentNode->testPackets.end())
	{
		cout.precision(7); 
		cout<<std::fixed<<"NodeIndex: "<<nodeIndex <<"  testPackets[" << index<<"]: "<<(*packetIterator)<<std::endl;
		packetIterator++;
		index++;	
	}

}

/*
 * Find next transmitting node by comparing packet transmitting timestamps
 * a: List of nodes transmitting on the network
 * simTime: Timestamp when simulation ends
 * returnMinTime: Timestamp of next transmitted packet
 * Return:
 * 	Index of transmitting node in list of nodes in the network
 * 	[-1] -> Error
 */
int findCurrentTransmitNode (list<node> &network, double simTime, double &returnMinTime)
{
	returnMinTime = simTime;
	int index = -1, debugIndex = 0;
	double currentPacketTime;
	list<node>::iterator currentNode = network.begin();

	while (currentNode != network.end())
	{
		if (currentNode->nodeSize() > 0)
		{	
			currentPacketTime = currentNode->getFirstPacket();

			if (currentPacketTime < returnMinTime)
			{
				index = distance(network.begin(), currentNode);
				returnMinTime = currentPacketTime;
			}
		}
		currentNode++;
		debugIndex++;
	}
	return index;
}


int main(){
	int packetLength = 1500, numNodes = 20, arrivalRate = 7;
	int currentTransmitNodeIndex = 0;
	double transDelay = 0.0015, propDelay = 0.00000005; 
	double currentTime = 0.0, simTime = 1000.0, currentTransmitTime = 0.0;
	bool collisionResult = false, busyResult = false;  
	bool senderNodeCollision = false, persistent = true;
	int index = 0, numTransmittedPackets = 0, numTotalPackets = 0, numTemptTransCounter = 0;

	uniform_real_distribution<double> dis(0, 1);

	default_random_engine gen;	
	list<node> nodeList;
	list<node>::iterator nodeIterator = nodeList.begin();

	ofstream persistentresults;
	persistentresults.open("persistent_results.csv");

	ofstream nonpersistentresults;
	nonpersistentresults.open("nonpersistent_results.csv");

	persistentresults<<"Arrival Rate, numNodes, Efficiency, ThroughPut\n";
	nonpersistentresults<<"Arrival Rate, numNodes, Efficiency, Throughput\n";
	for(int i = 0; i < 2; i++){
		arrivalRate = 7;
		collisionResult = false;
		busyResult = false;

		while(arrivalRate < 21){
			numNodes = 20;
			while (numNodes <= 100)
			{
				nodeList.clear();
				numTotalPackets = 0;
				numTransmittedPackets = 0;
				senderNodeCollision = false;
				currentTime = 0;
				currentTransmitTime = 0;
				numTemptTransCounter = 0; 

				for(int i = 0; i < numNodes; i++){
					nodeList.push_back(node(simTime, arrivalRate, gen, dis, persistent));
					node lastnode = nodeList.back();
				}

				currentTransmitNodeIndex = findCurrentTransmitNode(nodeList, simTime, currentTransmitTime);
				while (currentTransmitNodeIndex != -1 && currentTime < simTime)
				{
					senderNodeCollision = false;
					collisionResult = false;
					nodeIterator = nodeList.begin();
					index = distance(nodeList.begin(), nodeIterator);
					while (nodeIterator != nodeList.end())
					{
						if (index != currentTransmitNodeIndex && nodeIterator->nodeSize() > 0)
						{
							double currentPacketSenseTime = abs(index - currentTransmitNodeIndex) * propDelay + currentTransmitTime;
							collisionResult = nodeIterator->checkCollision(currentPacketSenseTime, transDelay, gen);
							if(collisionResult)
								numTemptTransCounter++; 

							senderNodeCollision = (collisionResult || senderNodeCollision);
						}
						index++;
						advance(nodeIterator, 1);
					}

					numTemptTransCounter++; 

					nodeIterator = nodeList.begin();
					advance(nodeIterator, currentTransmitNodeIndex);
					if(senderNodeCollision){
						nodeIterator->handleCollision(true, currentTransmitTime, gen, transDelay);
					}
					else if(!senderNodeCollision){

						numTransmittedPackets++;
						if(nodeIterator->nodeSize() > 0)
							nodeIterator->removeFirstPacket(false);	
						//Check busy condition for other nodes in network
						nodeIterator = nodeList.begin();
						index = distance(nodeIterator, nodeList.begin());	
						while(nodeIterator != nodeList.end()){
							busyResult = false; 
							if(index != currentTransmitNodeIndex){
								double currentPacketSenseTime = abs(index - currentTransmitNodeIndex) * propDelay + currentTransmitTime; 
								busyResult = nodeIterator->checkBusy(currentPacketSenseTime, transDelay, gen);
							}
							advance(nodeIterator, 1);
							index++; 
						}

					}
					numTemptTransCounter++;
					currentTransmitNodeIndex = findCurrentTransmitNode(nodeList, simTime, currentTransmitTime);
					currentTime = currentTransmitTime;
				}

				nodeIterator = nodeList.begin();
				while(nodeIterator != nodeList.end()){
					numTotalPackets += nodeIterator->temptCounter;
					advance(nodeIterator, 1);
				}

				cout<<"Numebr of Nodes: "<<numNodes<<" Num Total Packets: "<< numTemptTransCounter <<endl<< "Num Transmitted Packets: " << numTransmittedPackets <<endl<< "Efficiency: "<< ((double)numTransmittedPackets / numTemptTransCounter) <<endl<<"Throughput: "<< ((double)packetLength * numTransmittedPackets)/simTime/1000000 << "\n";
				if(persistent){
					persistentresults<<arrivalRate<<","<<numNodes<<","<<((double)numTransmittedPackets / numTotalPackets)<<","<<((double)packetLength * numTransmittedPackets / (simTime * 1000000))<<"\n";
	
				}
				else{
					nonpersistentresults<<arrivalRate<<","<<numNodes<<","<<((double)numTransmittedPackets / numTotalPackets)<<","<<((double)packetLength * numTransmittedPackets / (simTime * 1000000))<<"\n";
 
				}
				numNodes+=20;
			}
			if(arrivalRate == 7)
				arrivalRate = 10;
			else if(arrivalRate == 10)
				arrivalRate = 20;
			else if (arrivalRate == 20)
				arrivalRate = 30;
		}
		persistent = false;
	}

}

